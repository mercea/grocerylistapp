var express = require('express');
var mongoose = require('mongoose');
var UserTrend = require('../models/UserTrend');

var router = express.Router();

/* USER SETTINGS */
router.route('/')

    // Get user settings
    .get(function(req, res){
        console.log("Getting user settings");
        UserTrend.findOne({'email' : req.param('email')}, {'settings':1}, function(err, user){
            if(err){
                console.log("Error in getting user settings");
                res.send(err);
            }else{
                if(user){
                    console.log("Found user settings " + JSON.stringify(user.settings));
                    res.json(user.settings);
                }else{
                    console.log("user does not exist");
                    res.json({
                        status: 400,
                        message: "User not found"
                    });
                }
            }
        });
    })

    // Update user settings
    .put(function(req, res){
        console.log("Updating user settings");
        UserTrend.findOne({'email' : req.param('email')}, function(err, user){
            if(err){
                console.log("Error in getting user settings");
                res.send(err);
            }else{
                if(user){
                    console.log("Found user settings");
                  /*  var newSettings = new Settings();
                    newSettings.shoppingDay = req.body.shoppingDay;
                    newSettings.shoppingFrequency = req.body.shoppingFrequency;
                    newSettings.reminderEmails  = req.body.reminderEmails;*/
                    user.settings = req.body.newSettings;
                    user.save(function(err){
                        if(err){
                            res.send(err);
                        }else{
                            res.json({message: "User Settings successfully updated!"});
                        }
                    });
                }else{
                    console.log("user does not exist");
                    res.json({
                        status: 400,
                        message: "User not found"
                    });
                }
            }
        });
    });

module.exports = router;