var express = require('express');
var mongoose = require('mongoose');
var FrequentItems = require('../models/Groceries');
var UserTrend = require('../models/UserTrend');
var PastLists = require('../models/PastGroceryLists');

var router = express.Router();

/* PAST GROCERY LISTS */
router.route('/')	
	
	// Get all grocery lists
	.get(function(req, res){
		res.render('index', { title: 'My Lazy Grocery App!' });
})/*(function(req, res){
		UserTrend.find({username: res.body.username}, 'pastLists', function(err, items){
			if(err)
				res.send(err);
			res.json(items);
		});
	}) */
	
	//add new list to past  grocery lists
	.put(function(req, res){
		UserTrend.find({username: res.body.username}, function(err, item){
			if(err)
				res.send(err);
			// add grocery list to array of past lists
			var newList = new PastLists();
			newList.dateCreated = res.body.dateCreated;
			newList.list = res.body.groceryList;
			item.pastLists.push(newList);
			
			item.save(function(err){
				if(err)
					res.send(err);
				res.json({message: "Item successfully updated!"});
			});
		});
	});
	
router.route('/:list_id')	
	// Get a grocery list 
	.get(function(req, res){
		UserTrend.findById(req.params.list_id, function(err, item){
			if(err)
				res.send(err);
			res.json(item);
		});
	})
	
	// Edit a grocery list
	.put(function(req, res){
		UserTrend.findById(req.params.list_id, function(err, item){
			if(err)
				res.send(err);
			// do editing 
			res.json({message: "grocery list updated"});
		})
	})	
	
	// delete a grocery list
	.delete(function(req, res){
		UserTrend.remove({
			_id: req.params.list_id
		}, function(err, item){
			if(err)
				res.send(err);
			res.json({message: "grocery list  deleted"});		
		});
	});

module.exports = router;	