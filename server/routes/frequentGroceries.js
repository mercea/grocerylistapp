var express = require('express');
var mongoose = require('mongoose');
var FrequentItems = require('../models/Groceries');
var UserTrend = require('../models/UserTrend');

var router = express.Router();

/* FREQUENTLY SHOPPED ITEMS */

router.route('/')
	// Create a new frequently shopped item
	.post(function(req, res){
		// new instance of frequently used groceries
		var user = new UserTrend();
		user.email = req.param('email');

		console.log("Adding a new frequently bought grocery item");
		user.frequentItems.push(req.param('item'));
		user.save(function(err){
			if(err)
				res.send(err);
			console.log("new frequent grocery with name " + req.body.grocery + " was created");
			res.json({ message: "You've added a new grocery item to your most loved groceries list"});
		});
	})
	
	// Get all frequently shopped items
	.get(function(req, res){
		UserTrend.findOne({'email': req.param('email')}, {'frequentItems':1}, function(err, items){
			console.log("username is " +  req.param('email'));
			if(err) {
				console.log("Error in getting groceries for " + err);
				res.send(err);
			}
			else{
				if(items){
					console.log("Gotten grocery list " + JSON.stringify(items.frequentItems));
					res.json(items.frequentItems);
				}else{
					console.log("user does not exist");
					res.json({
						status: 400,
						message: "User not found"
					});
				}

			}
		});
	})

	// Edit frequently shopped items
	.put(function(req, res){
		UserTrend.findOne({'email': req.body.email}, {'frequentItems':1}, function(err, list){
			console.log("Email to be updated is " +  req.body.email);
			console.log("items to be added to the list are " + req.body.item);
			if(err){
				console.log("Error occurred in updating frequent items" + err);
				res.send(err);
			}else{
				console.log(JSON.stringify(req.body.item));

				list.frequentItems.push(req.body.item);
				list.save(function(err){
					if(err){
						res.send(err);
					}else{
						res.json({message: "Item successfully updated!"});
					}
				});
			}
		});
	});

// Get a single frequently shopped item
router.route('/FrequentGroceries:grocery_id')
	.get(function(req, res){
		UserTrend.findById(req.params('grocery_id'), function(err, item){
			if(err)
				res.send(err);
			res.json(item);
		});
	})

// Edit a frequently shopped item 	
	.put(function(req, res){
		UserTrend.findById(req.body.grocery_id, function(err, item){
			if(err) {
				res.send(err);
			}else {
				// update whatever needs updating
				item.grocery = res.body.grocery;

				item.save(function (err) {
					if (err)
						res.send(err);
					res.json({message: "Item successfully updated!"});
				});
			}
		});
	})	
	
// delete a frequently shopped item
	.delete(function(req, res){
		UserTrend.remove({
			_id: req.params.grocery_id
		}, function(err, item){
			if(err)
				res.send(err);
			res.json({message: "item deleted"});		
		});
	});
	
module.exports = router;
