var express = require('express');
var mongoose = require('mongoose');
var jwt = require('jwt-simple');

var User = require('../models/User');
var UserTrend = require('../models/UserTrend');
var Settings = require('../models/Settings');
var secret = require('../config/secret')();

var router = express.Router();

/* AUTHENTICATION ROUTES */
router.get('/', function(req, res) {
  res.render('index', { title: 'My Lazy Grocery App!' });
});

/* Authenticate User */
router.post('/', function(req, res){
	console.log('request to login was made from ' + req.body.email);
	var email = req.body.email || '';
	var password = req.body.password || '';
	
	if(email == '' || password ==''){
		res.status(401);
		res.json({
			status: 401,
			message: "Invalid Credentials"
		});
		console.log('Invalid Credentials' + req.body.email);
		return;
	}
	// Check the database for email/password combination
	User.findOne({email: req.body.email}, function(err, user){
		console.log(user);
		if(err){
			console.log('Error in checking BD for user');
			res.json({
				type: false,
				message: "Error occurred: " + err
			});
		}else{
			if(user){
				console.log('User found with email ' + req.body.email);
				if(user.validatePassword(req.body.password)){
					console.log('user found ' + user);
					res.json(genToken(user));
				}
				else{
					console.log('Wrong password entered for ' + req.body.email + " password: " + req.body.password);
				}
			}else{
				console.log('No User found with email ' + req.body.email);
				res.json({
					status: 401,
                    type: false,
                    message: "Incorrect email/password"
                });
			}			
		}	
	});
});

/* Create new account if not exists
 accessed by /login/signup */
router.post('/signup', function(req, res) {
	var email = req.body.email || '';
	var password = req.body.password || '';
	var username = req.body.username || '';
	
	if(email == '' || password =='' || username == ''){
		res.status(401);
		res.json({
			status: 401,
			message: "Invalid Credentials"
		});
		return;
	}
    User.findOne({email: req.body.email}, function(err, user) {
		console.log("Request to create account for " + req.body.email );
        if (err) {
            res.json({
				status: 400,
                type: false,
                message: "Error occurred: " + err
            });
        } else {
            if (user) {
                res.json({
					status: 401,
                    type: false,
                    message: "User already exists!"
                });
            } else {
				// Create a new user in the DB
                var userModel = new User();
				userModel.username = req.body.username;
                userModel.email = req.body.email;
                userModel.password = userModel.generateHash(req.body.password);
                userModel.save(function(err, user) {
					if(err){
						console.log("Error in getting data from Db " + err);
						res.json({
							status: 400,
							type: false,
							message: "Error occurred: " + err
						});
					}else{
						// Create a new user trend entry in DB for new user
						console.log("Creating a new user trend entry in DB for new user");
						var userTrendModel = new UserTrend();
						userTrendModel.email = req.body.email;
						userTrendModel.frequentItems = [];
						userTrendModel.pastLists = [];
						//var defaultSettings = new Settings();
						//	defaultSettings.shoppingDay = "Friday";
						//	defaultSettings.shoppingFrequency  = "weekly";
						//	defaultSettings.reminderEmails = true;
						userTrendModel.settings = new Settings();

						userTrendModel.save(function (err, userTrend){
							if(err){
								console.log("Error in creating new user trend in Db " + err);
								res.json({
									status: 400,
									type: false,
									message: "Error occurred: " + err
								});
							} else {
								console.log("Successfully created a new user and new entry in User trends DB");
								res.json({
									status: 200,
									type: true,
									data: user
								});
							}
						});
					}
                    });
                }
            }
		});
    });

	
// Generate token to be returned if authenticated
function genToken(user){
	var expires = expiresIn(7);
	var token = jwt.encode({
		exp: expires
	}, secret);
	
	return {
		token: token,
		expires: expires,
		user: user
	};
}

// Set expiry date 7 days from current date
function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = router;
