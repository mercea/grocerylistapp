var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// routes
var routes = require('./routes/index');
var freq = require('./routes/frequentGroceries');
var auth = require('./routes/auth');
var pastLists = require('./routes/PastGroceryLists');
var currentList = require('./routes/currentGroceryList');
var settings = require('./routes/settings');

var mongo = require('mongodb');
var mongoose = require('mongoose');
mongoose.connect('mongodb://admin:LazyGroceryApp@ds031691.mongolab.com:31691/lazygroceryapp/');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.all('/*', function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	
	if (req.method == 'OPTIONS') {
    res.status(200).end();
	} else {
    next();
  }	
});

// All requests starting with /api/* should be checked for token
app.all('/api/*', [require('./middleware/validateRequest')]);
app.use('/api/', routes);
app.use('/api/frequentGroceries/', freq);
app.use('/api/pastGroceryLists/', pastLists);
app.use('/api/currentGroceryList/', currentList);
app.use('/api/Settings/', settings);
app.use('/login/', auth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

var port = process.env.PORT || 8080;

app.listen(port);
console.log('Fun happens on port ' + port);

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        console.log(err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
