var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FrequentGrocery = require('./Groceries.js');
var FrequentGrocerySchema = FrequentGrocery.Schema;

var PastGroceryListSchema = new Schema({
	dateCreated : {type : Date, default: Date.now},
	list : [FrequentGrocerySchema]
});

module.exports = mongoose.model('PastGroceryLists', PastGroceryListSchema);
