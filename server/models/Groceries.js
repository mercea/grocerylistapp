var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Types.ObjectId;

var GrocerySchema = new Schema({
	_id: {
		type: Schema.ObjectId, default: function () {
			return new ObjectId()
		}
	},
	grocery: String,
	quantity: Number,
	measure: String,
	frequency: String,
	interval: String,
	lastBought: {type: Date, default: Date.now},
	updated: {type: Date, default: Date.now},
	category: {type: String, default: "None"}
});

module.exports = mongoose.model('Groceries', GrocerySchema);