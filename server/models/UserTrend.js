var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FrequentGrocery = require('./Groceries.js');
var FrequentGrocerySchema = FrequentGrocery.Schema;
var Settings = require('./Settings.js');
var SettingsSchema = Settings.Schema;
var OldList = require('./PastGroceryLists.js');
var OldListSchema = OldList.Schema;

/*Model for user settings */
var UserTrendSchema = new Schema({
	email : String,
	frequentItems : [FrequentGrocerySchema],
	pastLists : [OldListSchema],
	settings  : {type: Schema.Types.ObjectId, ref: 'Settings'}
},
{collection : 'UserTrends'});

module.exports = mongoose.model('UserTrend', UserTrendSchema);