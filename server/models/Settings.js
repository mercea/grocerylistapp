var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*Model for user settings */
var SettingsSchema = new Schema({
	shoppingDay: {type: String, default : "Friday"},
	shoppingFrequency : {type: String, default : "weekly"},
	reminderEmails : {type: Boolean, default : true}
});

module.exports = mongoose.model('Settings', SettingsSchema);