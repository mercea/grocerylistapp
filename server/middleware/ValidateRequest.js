var jwt = require('jwt-simple');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var secret = require('../config/secret.js')();
var User = require('../models/User');

module.exports = function(req, res, next){

	var token = (req.body && req.body.access_token) || (req.query && req.query.access_token || req.headers['x-access-token']);
	var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];

	if(token || key){
		try{
			var decodedToken = jwt.decode(token, secret);
			console.log("Decoded Token " + decodedToken + " Key " + key);

			if(decodedToken <= Date.now()){
				console.log("Token is expired");
				res.status(400);
				res.json({
					status: 400,
					message: "Token Expired"
				});
				return;
			}
			
			// Authorize user if allowed access to resources. Key is user email
			User.findOne({email: key}, function(err, user){
				console.log("I got here");
				if(err){
					console.log("Error in finding user " + err);
					res.json({
						type: false,
						data: "Error occurred: " + err
					});
				}else{
					if(user){
						console.log("I found a user with email " + key);
						next();
					}else{
						console.log("no user found ");
						// No user with this name exists, respond back with a 401
						res.status(401);
						res.json({
						  "status": 401,
						  "message": "Invalid User"
						});
						return;
					}					
				}
			});
		}catch (err) {
			console.log("Error in validating token " + err);
			  res.status(500);
			  res.json({
				"status": 500,
				"message": "Oops something went wrong",
				"error": err
			  });
    }
} else {
   // res.send(401);
    res.json({
      status: 401,
      message: "Invalid Token or Key"
    });
    return;
  }
};
