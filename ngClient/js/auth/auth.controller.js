myApp.controller('LoginCtrl', ['$scope', '$window', '$location', 'UserAuthFactory', 'AuthenticationFactory',
  function($scope, $window, $location, UserAuthFactory, AuthenticationFactory) {
    $scope.login = function() {
 
      var email = $scope.user.email,
        password = $scope.user.password;
 
      if (email !== undefined && password !== undefined) {
        UserAuthFactory.login(email, password).success(function(data) {
       
          AuthenticationFactory.isLogged = true;
          AuthenticationFactory.user = data.user.email;
 
          $window.sessionStorage.token = data.token;
          $window.sessionStorage.user = data.user.email; // to fetch the user details on refresh
 
          $location.path("/");
 
        }).error(function(status) {
          alert('Oops something went wrong!');
        });
      } else {
        alert('Invalid credentials');
      } 
    }; 
  }
]);

myApp.controller("SignupCtrl", ['$scope', '$location', 'UserAuthFactory',
  function($scope, $location, UserAuthFactory){
    $scope.signup = function(){
      var username = $scope.user.username,
          email= $scope.user.email,
          password = $scope.user.password,
          verifyPassword = $scope.verifyPassword;
      if(password !== verifyPassword){
        alert("Passwords don't match!");
        return;
      }
      if(username !== undefined && password !== undefined){
        UserAuthFactory.signup(email, username, password).success(function(data){
          $location.path("/");
        }).error(function(status){
          alert("hmmm...could not sign you in. Try again");
        });
      }
      else{
        alert("The creds provided are not valid");
      }
    }
  }
]);