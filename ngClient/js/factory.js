// Master Grocery List factory
myApp.factory('GrocerylistsFactory', function($http) {
  var urlBase = 'http://localhost:8080/api/groceryLists';
  var _listsFactory = {};

// Get all old lists
  _listsFactory.getOldLists = function() {
    return $http.get(urlBase, username);
  };

  // add list to lists
  _listsFactory.updateTodo = function(todo) {
    return $http.put(urlBase, todo);
  };

  // delete a single list
  _listsFactory.deleteList = function(id) {
    return $http.delete(urlBase + '/' + id);
  };

  return _listsFactory;
});

// Frequent Groceries factory
myApp.factory('FrequentItemsFactory', function($http, $window) {
  var urlBase = 'http://localhost:8080/api/frequentGroceries/';
  var itemsFactory = {};

  // get all frequent grocery items
  itemsFactory.getItems = function() {
    return $http.get(urlBase, {params: {email: $window.sessionStorage.user}});
  };

  itemsFactory.getSingleItem = function() {
    return $http.get(urlBase);
  };

  itemsFactory.saveItems = function(todo) {
    return $http.post(urlBase, todo);
  };

  // add an item to frequent grocery list
  itemsFactory.updateItems = function(grocery, quantity, measure, interval, lastBought, category){
    newItem = CreateGroceryItem(grocery, quantity, measure, interval, lastBought, category);
    return $http.put(urlBase, {item: newItem, email: $window.sessionStorage.user});
  };

  // update a single grocery item
  itemsFactory.updateSingleItem = function(id, grocery, quantity, measure, interval, lastBought, category){
    updatedItem = CreateGroceryItem(grocery, quantity, measure, interval, lastBought, category);
    return $http.put(urlBase + '/' + id, {item: updatedItem, email: $window.sessionStorage.user});
  };

  // delete a single grocery item
  itemsFactory.deleteSingleItem = function(id) {
    return $http.delete(urlBase + '/' + id);
  };

  return itemsFactory;
});


// Settings factory
myApp.factory('SettingsFactory', function ($http, $window) {
  var urlBase = 'http://localhost:8080/api/settings/';
  var settingsFactory = {};

  // get user settings
  settingsFactory.getSettings = function(){
    return $http.get(urlBase, {params: {email: $window.sessionStorage.user}});
  };

  return settingsFactory;
});

// Method to create Grocery Item
function CreateGroceryItem(grocery, quantity, measure, interval, lastBought, category){
  var item = {
    "grocery" : grocery,
    "quantity" :quantity,
    "measure" : measure,
    "interval" : interval,
    "lastBought" : lastBought,
    "category" : category
  };
  console.log(item);
  return item;
}

