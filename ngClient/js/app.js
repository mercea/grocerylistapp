var myApp = angular.module('grocerylist', ['ngRoute']);

myApp.config(function($routeProvider, $httpProvider) {
  
  $httpProvider.interceptors.push('TokenInterceptor');
  
  $routeProvider
	  .when('/login', {
		  templateUrl: 'partials/login.html',
		  controller: 'LoginCtrl',
		  access: {
			  requiredLogin: false
		  }
	  })
    .when('/', {
      templateUrl: 'partials/home.html',
      controller: 'HomeCtrl',
		access: {
			requiredLogin: true
		}
    })
	.when('/GroceryLists', {
      templateUrl: 'partials/GroceryLists.html',
      controller: 'GroceryListsCtrl',
		access: {
			requiredLogin: true
		}
    })
	.when('/GroceryLists/:id', {
      templateUrl: 'partials/GroceryListDetail.html',
      controller: 'GroceryListDetailCtrl',
		access: {
			requiredLogin: true
		}
    })
	.when('/GroceryList/current', {
      templateUrl: 'partials/CurrentList.html',
      controller: 'CurrentListCtrl',
		access: {
			requiredLogin: true
		}
    })
	.when('/Frequent', {
		templateUrl: 'partials/FrequentItems.html',
		controller: 'FreqItemsCtrl',
		access: {
			requiredLogin: true
		}
	})
	.when('/Frequent/:id', {
		templateUrl: 'partials/FreqItemDetail.html',
		controller: 'FreqItemDetailCtrl',
		access: {
			requiredLogin: true
		}
	})
	  .when('/AddFrequentItem/', {
		  templateUrl: 'partials/AddFreqItem.html',
		  controller: 'AddFreqItemCtrl',
		  access: {
			  requiredLogin: true
		  }
	  })
	.when('/signup', {
		templateUrl: 'partials/signup.html',
		controller: 'SignupCtrl',
		access: {
			requiredLogin: false
		}
	})
  	.when('/settings', {
	    templateUrl: 'partials/settings.html',
	    controller: 'SettingsCtrl',
	    access: {
		    requiredLogin: true
	    }
  })
	  .when('/EditSettings', {
		  templateUrl: 'partials/editSettings.html',
		  controller: 'EditSettingsCtrl',
		  access: {
			  requiredLogin: true
		  }
	  })
	.otherwise({
      redirectTo: '/login'
    });
});

myApp.run(function($rootScope, $window, $location, AuthenticationFactory) {
  // when the page refreshes, check if the user is already logged in
  AuthenticationFactory.check();
 
  $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
    if ((nextRoute.access && nextRoute.access.requiredLogin) && !AuthenticationFactory.isLogged) {
      $location.path("/login");
    } else {
      // check if user object exists else fetch it. This is incase of a page refresh
      if (!AuthenticationFactory.user) AuthenticationFactory.user = $window.sessionStorage.user;      
    }
  });
 
  $rootScope.$on('$routeChangeSuccess', function(event, nextRoute, currentRoute) {
    $rootScope.showMenu = AuthenticationFactory.isLogged;    
    // if the user is already logged in, take him to the home page
	   if (AuthenticationFactory.isLogged == true && $location.path() == '/login') {
      $location.path('/');
    }
  });
});
