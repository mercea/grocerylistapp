// Header Controller
myApp.controller("HeaderCtrl", ['$scope', '$location', 'UserAuthFactory',
  function($scope, $location, UserAuthFactory) {
    $scope.isActive = function(route) {
      return route === $location.path();
    }

    $scope.logout = function(){
      UserAuthFactory.logout();
    }
  }
]);

// Home Controller
myApp.controller("HomeCtrl", ['$scope',
  function($scope) {
    $scope.name = "Home Controller";
	$scope.title = "You'll be running low on...";
  }
]);

// Grocery lists Controller
myApp.controller("GroceryListsCtrl", ['$scope', 'GroceryListsFactory',
  function($scope, GroceryListFactory) {
    $scope.name = "Grocery Lists Controller";
    $scope.lists = [];

    // Get all groceries for current user
    GroceryListFactory.getOldLists().then(function(response){
      $scope.lists = response.data[0]['pastLists'];
      console.log("Gotten past lists " + $scope.lists);
   });
  }
]);

//
myApp.controller("GroceryListDetailCtrl", ['$scope',
  function($scope) {
    $scope.name = "Grocery List Detail Controller";
    // Below data will be used by checkmark filter to show a ✓ or ✘ next to it
    $scope.list = ['yes', 'no', true, false, 1, 0];
  }
]);

// Current grocery List controller
myApp.controller("CurrentListCtrl", ['$scope', 'todosFactory',
  function($scope, todosFactory) {
    $scope.name = "Current Grocery List Controller";
    $scope.todos = [];

    // Access the factory and get the latest Todos list
    todosFactory.getTodos().then(function(data) {
      $scope.todos = data.data;
    });

  }
]);

// Frequent grocery items Controller
myApp.controller("FreqItemsCtrl", ['$scope', 'FrequentItemsFactory',
	function($scope, FrequentItemsFactory){
      $scope.name = "Frequent groceries Controller";
      $scope.groceries = [];
      $scope.message = "";

      // Get all groceries for current user
      FrequentItemsFactory.getItems().then(function(response){
        var data = response.data;
          if(data.length == 0){
              $scope.message = "You have no items to display. Click edit to add items to your list";
          }else{
              $scope.groceries = data;
          }
        console.log("this is " + $scope.groceries);
      });
	}
]);

// Controller for adding more items to frequent grocery list
myApp.controller("AddFreqItemCtrl", ['$scope', 'FrequentItemsFactory', '$location',
  function($scope, FrequentItemsFactory, $location){
   $scope.updateItems = function(){
      var grocery = $scope.grocery,
          quantity = $scope.quantity,
          measure = $scope.measure,
          interval = $scope.interval,
          lastBought = $scope.lastBought,
          category = $scope.category;
      console.log("Adding a new item");
     console.log(grocery + quantity + measure + interval + lastBought + category);

      if(grocery !== undefined && quantity !== undefined && measure !== undefined && interval !== undefined && lastBought!== undefined && category !== undefined){
        FrequentItemsFactory.updateItems(grocery, quantity, measure, interval, lastBought, category)
            .success(function(data){
              $location.path("/AddFrequentItem");
            });

      }else{
        console.log("Missing field " + grocery + quantity + measure + interval + lastBought + category);
      }
    };
    // Get all groceries for current user
    FrequentItemsFactory.getItems().then(function(response){
      $scope.groceries = response.data;
      console.log("this is " + $scope.groceries);
    });
  }
]);

// Controller for editing a grocery item
myApp.controller("EditFreqItemCtrl", ['$scope', 'FrequentItemsFactory', '$location',
   function($scope, FrequentItemsFactory, $location){
       $scope.saveEdit = function(id){
           var grocery = $scope.grocery,
               quantity = $scope.quantity,
               measure = $scope.measure,
               interval = $scope.interval,
               lastBought = $scope.lastBought,
               category = $scope.category;
           console.log("Adding a new item");
           console.log(grocery + quantity + measure + interval + lastBought + category);

           if(grocery !== undefined && quantity !== undefined && measure !== undefined && interval !== undefined && lastBought!== undefined && category !== undefined){
               FrequentItemsFactory.updateSingleItem(id, grocery, quantity, measure, interval, lastBought, category)
                   .success(function(data){
                       $location.path("/AddFrequentItem");
                   });

           }else{
               console.log("Missing field " + grocery + quantity + measure + interval + lastBought + category);
           }
           // Get all groceries for current user
           FrequentItemsFactory.getItems().then(function(response){
               $scope.groceries = response.data;
               console.log("this is " + $scope.groceries);
           });
       }
   }
]);

// Controller for deleting a grocery item
myApp.controller("DelFreqItemsCtrl", ['$scope',
    function($scope){
        $scope.name = "Delete";
    }
]);

// Settings Controller
myApp.controller("SettingsCtrl", ['$scope', 'SettingsFactory','$location',
    function($scope, SettingsFactory, $location) {
        SettingsFactory.getSettings().then(function (response){
            console.log("Settings gotten back from the service");
            $scope.shopnFreq = response.shoppingFrequency;
            $scope.shopnDay = response.shoppingDay;
            $scope.emailReminder = response.reminderEmails;
        });
    }
]);

// Edit Settings Controller
myApp.controller("EditSettingsCtrl", ['$scope', 'SettingsFactory','$location',
  function($scope, SettingsFactory, $location){
    $scope.name = "Settings";
      $scope.saveSettings = function(){
          var shopnFreq = $scope.shopnFreq,
              shopnDay = $scope.shopnDay,
              emailReminder = $scope.emailReminder;
          if(shopnFreq !== undefined && shopnDay  !== undefined && emailReminder !== undefined){
              SettingsFactory.UpdateSettings(shopnFreq, shopnDay, emailReminder)
                  .success(function(data){
                     $location.path("/settings");
                  });
              }
          };
      $scope.cancelEdit = function(){
          $location.path("/settings");
      };
    }
]);